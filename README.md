# multi_load_conf

a simple python script which sends config to a group of hosts specified in a yaml file.

## Getting started

Install netmiko via `pip install -r requirements.txt`

Change the username and password in the `ch` dictionary.  device-type and host will be pulled from the yaml file.

Prepare a yaml file with the hostnames followed by a list of config you wish to send to said hostname. For example:

```
---
sw1.example.com
  platform: 'ios'
  config:
    - hostname sw1.example.com
    - ip ssh version 2
sw2.example.com
  platform: 'eos'
  config:
    - hostname sw2.example.com
    - interface Gi1/0/1
    - desc some wallport
    - ip ssh version 2
```

Once yaml file is composed run the following:

```
./multi_load_conf -f host_cmds.yml
```