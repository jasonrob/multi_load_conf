#!/usr/bin/env python3

import yaml
import argparse
import getpass
from netmiko import ConnectHandler
from netmiko.ssh_exception import *

parser = argparse.ArgumentParser(description='Load yaml file and deploy list of commands to hostnames')
parser.add_argument('-f', dest='file', type=str, help='provide path to yaml file')
parser.add_argument('-u', dest='miko_user', type=str, help='provide user')
parser.add_argument('-p', dest='miko_pass_set', action='store_true', help='prompt for password')
args = parser.parse_args()

def confirm_password(pass1,pass2):
    if pass1 == pass2:
        return True
    else:
        return False

if args.miko_pass_set and args.miko_user:
    miko_user = args.miko_user
    miko_pass1 = getpass.getpass(prompt='Password: ', stream=None)
    miko_pass2 = getpass.getpass(prompt='Retype password: ', stream=None)
    
    if confirm_password(miko_pass1,miko_pass2):
        miko_pass = miko_pass1
    else:
        print('\033[31mPasswords do not match, try again.\033[0m')
        exit()

else:
    miko_user = 'joe'
    miko_pass = 'blow'

def netmiko_platform(platform):
    if platform == 'ios':
        return 'cisco_ios'
    elif platform == 'eos':
        return 'arista_eos'
    elif platform == 'nxos':
        return 'cisco_nxos'
    elif platform == 'ubnt':
        return 'ubiquiti_edgerouter'
    else:
        return 'cisco_ios' # set this to your dominate platform

ch = { 
    'device_type': '',
    'host': '',
    'username': miko_user,
    'password': miko_pass
}

with open (args.file, 'r') as f:
    host_cmds = yaml.safe_load(f)

for host in host_cmds:
    
    print('\033[92mHostname: {} \033[0m \n'.format(host))

    print('Config Dump:\n')
    print('\n'.join(map(str, host_cmds[host]['config'])))
    
    ch['host'] = host
    ch['device_type'] = netmiko_platform(host_cmds[host]['platform'])

    try:
        netmiko = ConnectHandler(**ch)
    except (EOFError, SSHException) as e:
        print('\033[31m' + host + ' has encoutered an error.\033[0m')
        print(e)
        continue
    
    output = netmiko.send_config_set(host_cmds[host]['config'])
        
    netmiko.save_config()
    netmiko.disconnect()

    print('\nResults from config dump:\n')
    print(output)